# TobliTof

TobliKaDjate est un site web pour les photographes, les illustrateur, les maquettistes, les architectes, les artisans, les artistes du Mali dans le but d'exposer leurs produits, leurs créations.

## Environnement de développement

### Pré-requis

* PHP 7.4
* Composer
* Symfony CLI

### Vous pouvez verifier les pré-requis avec la commande suivante (de la CLI Symfony) :

```bash
symfony check:requirements
```

### Lancer l'environnement de développement

```bash
symfony serve -d
```

