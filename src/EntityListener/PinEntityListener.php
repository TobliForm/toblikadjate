<?php

namespace App\EntityListener;

use App\Entity\Pin;
use Doctrine\ORM\Event\LifecycleEventArgs;
use Symfony\Component\String\Slugger\SluggerInterface;

class PinEntityListener
{
    private $slugger;

    public function __construct(SluggerInterface $slugger)
    {
        $this->slugger = $slugger;
    }

    public function prePersist(Pin $pin, LifecycleEventArgs $event)
    {
        $slug = $this->slugger->slug($pin->getTitle());
        $pin->setSlug($slug);
    }

    public function preUpdate(Pin $pin, LifecycleEventArgs $event)
    {
        $slug = $this->slugger->slug($pin->getTitle());
        $pin->setSlug($slug);
    }
}
