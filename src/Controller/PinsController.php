<?php

namespace App\Controller;

use App\Entity\Pin;
use App\Form\PinType;
use App\Repository\PinRepository;
use Doctrine\ORM\EntityManagerInterface;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;

class PinsController extends AbstractController
{
    /**
     * @Route("/", name="app_home", methods={"GET"})
     */
    public function index(PinRepository $pinRepo, Request $request, PaginatorInterface $paginator): Response
    {
        $data = $pinRepo->findBy([], ['createdAt' => 'DESC']);

        $pins = $paginator->paginate(
            $data,
            $request->query->getInt('page', 1), /*page number*/
            Pin::NBRE_ELE_PAR_PAGE /*limit per page*/
        );

        return $this->render('pins/index.html.twig', [
            'pins' => $pins,
        ]);
    }

    /**
     * @Route("/pins/create", name="app_pins_create", methods={"GET", "POST"})
     * @IsGranted("PIN_CREATE")
     */
    public function create(Request $request, EntityManagerInterface $em): Response
    {
        $pin = new Pin;
        $form = $this->createForm(PinType::class, $pin);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $pin = $pin->SetUser($this->getUser());
            $em->persist($pin);
            $em->flush();
            $this->addFlash('success', 'Votre création a été publiée avec succès!');
            return $this->redirectToRoute('app_home');
            
        }
        
        return $this->render('pins/create.html.twig', [
            'form' => $form->createView(),
            'current_menu' => 'new'
        ]);
    }

    /**
     * @Route("/pins/{slug}", name="app_pins_show", methods={"GET"})
     */
    public function show(Pin $pin): Response
    {
        return $this->render('pins/show.html.twig', [
            'pin' => $pin
        ]);
    }

    /**
     * @Route("/pins/{slug}/edit", name="app_pins_edit", methods={"GET", "PUT"})
     * @security("is_granted('PIN_MANAGE', pin)")
     */
    public function edit(Request $request, Pin $pin, EntityManagerInterface $em): Response
    {
        $form = $this->createForm(PinType::class, $pin, [
            'method' => 'PUT'
        ]);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
    
            $em->flush();
            $this->addFlash('success', 'Votre création a été modifiée avec succès!');
            return $this->redirectToRoute('app_home');
        }
        
        return $this->render('pins/edit.html.twig', [
            'form' => $form->createView(),
            'pin' => $pin,
        ]);
    }

    /**
     * @Route("/pins/{slug}", name="app_pins_delete", methods={"DELETE"})
     */
    public function delete(Request $request, Pin $pin, EntityManagerInterface $em): Response
    {
        $this->denyAccessUnlessGranted('PIN_MANAGE', $pin);
        
        if ($this->isCsrfTokenValid('pin_deletion_' . $pin->getId(), $request->request->get('csrf_token'))) {
            $em->remove($pin);
            $em->flush();
        }
        $this->addFlash('success', 'Votre création a été supprimée avec succès!');
        return $this->redirectToRoute('app_home');   
    }
}
